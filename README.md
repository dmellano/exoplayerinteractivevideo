# Interactive video player android application (Exoplayer)


### Dependencies used: 
| Name  |Use   |License   |
|---|---|---|
| LiveData (part of Android Jetpack)  |  Observable data holders |[Apache 2.0](https://android.googlesource.com/platform/frameworks/support/+/androidx-master-dev/LICENSE.txt/)   |
| Material Design  | UI - material components  | [Apache 2.0](https://github.com/material-components/material-components-android/blob/master/LICENSE/)  |
| Constraint Layout(part of Android Jetpack)  | UI component  | [Apache 2.0](https://mvnrepository.com/artifact/androidx.constraintlayout/constraintlayout/1.1.3) |
| Hilt  | Dependency Injection  |   |
| RxAndroid  | Reactive components  | [Apache 2.0](https://github.com/ReactiveX/RxAndroid#license)  |
| Coroutines  | Concurrency components  | [Apache 2.0](https://github.com/Kotlin/kotlinx.coroutines/blob/master/LICENSE.txt) |
