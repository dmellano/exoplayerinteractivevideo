package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager
import com.google.android.exoplayer2.ExoPlayer

class InitializePlayerUseCaseImpl constructor(private val playerManager: ExoplayerManager):InitializePlayerUseCase {
    override suspend fun initialize(): ExoPlayer? {
        return playerManager.initializePlayer()
    }
}