package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

interface VolumeUseCase {
    suspend fun volumeUp()
    suspend fun volumeDown()
}