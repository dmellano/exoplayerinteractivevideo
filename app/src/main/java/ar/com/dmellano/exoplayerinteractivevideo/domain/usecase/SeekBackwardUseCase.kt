package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

interface SeekBackwardUseCase {
    suspend fun seekBackward()
}