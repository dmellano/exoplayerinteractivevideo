package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

interface StopVideoUseCase {
    suspend fun stopVideo()
}
