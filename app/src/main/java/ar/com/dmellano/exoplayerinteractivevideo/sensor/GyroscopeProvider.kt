package ar.com.dmellano.exoplayerinteractivevideo.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.LifecycleOwner
import javax.inject.Inject

class GyroscopeProvider @Inject constructor(val context: Context) : SensorEventListener,
    LifecycleEventObserver {

    private val sensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val gyroscopeSensor: Sensor = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE)
    var orientationEventListener: OrientationListener? = null

    //The time-stamp being used to record the time when the last gyroscope event occurred.
    private var timestamp: Long = 0


    /**
     * Value giving the total velocity of the gyroscope (will be high, when the device is moving fast and low when
     * the device is standing still). This is usually a value between 0 and 10 for normal motion. Heavy shaking can
     * increase it to about 25. Keep in mind, that these values are time-depended, so changing the sampling rate of
     * the sensor will affect this value!
     */
    private var gyroscopeRotationVelocity = 0.0


    fun start() {
        sensorManager.registerListener(this, gyroscopeSensor, SensorManager.SENSOR_DELAY_GAME)
    }

    fun stop() {
        sensorManager.unregisterListener(this)
    }

    override fun onSensorChanged(event: SensorEvent) {
        // we received a sensor event. it is a good practice to check
        // that we received the proper event
        if (event.sensor.type == Sensor.TYPE_GYROSCOPE) {
            if (timestamp != 0L) {
                val dT = (event.timestamp - timestamp) * NS2S

                // Axis of the rotation sample, not normalized yet.
                var axisX = event.values[0]
                var axisY = event.values[1]
                var axisZ = event.values[2]
                // Calculate the angular speed of the sample
                gyroscopeRotationVelocity =
                    Math.sqrt((axisX * axisX + axisY * axisY + axisZ * axisZ).toDouble())

                //FIXME: remove this logging
                if (axisX.toInt() != 0 || axisY.toInt() != 0 || axisZ.toInt() != 0) {
                    Log.d(
                        TAG,
                        "onSensorChanged: \nX=${axisX.toInt()} \n Y°=${axisY.toInt()} \n Z°=${axisZ.toInt()}\n dT=$dT / gyroscopeRotationVelocity=$gyroscopeRotationVelocity"
                    )
                }
                orientationEventListener?.onOrientationUpdate(
                    axisX.toInt(),
                    axisY.toInt(),
                    axisZ.toInt()
                )
            }
            timestamp = event.timestamp
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        //Nothing to do here
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_RESUME -> start()
            Lifecycle.Event.ON_PAUSE -> stop()
            else -> {
            }
        }
    }

    companion object {
        private const val TAG: String = "GyroscopeProvider"

        //Constant specifying the factor between a Nano-second and a second
        private const val NS2S = 1.0f / 1000000000.0f
    }

}