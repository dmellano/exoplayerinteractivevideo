package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager

class VolumeUseCaseImpl constructor(private val playerManager: ExoplayerManager): VolumeUseCase {

    override suspend fun volumeUp() {
        playerManager.volumeUp()
    }

    override suspend fun volumeDown() {
        playerManager.volumeDown()
    }

}