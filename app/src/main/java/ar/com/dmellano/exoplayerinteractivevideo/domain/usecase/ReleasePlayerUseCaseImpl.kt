package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager

class ReleasePlayerUseCaseImpl constructor(private val playerManager: ExoplayerManager): ReleasePlayerUseCase {
    override suspend fun release() {
        playerManager.releasePlayer()
    }
}