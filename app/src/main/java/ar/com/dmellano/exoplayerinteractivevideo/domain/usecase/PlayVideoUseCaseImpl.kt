package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager

class PlayVideoUseCaseImpl constructor(private val playerManager: ExoplayerManager) :
    PlayVideoUseCase {

    override suspend fun playVideo() {
        playerManager.play()
    }
}