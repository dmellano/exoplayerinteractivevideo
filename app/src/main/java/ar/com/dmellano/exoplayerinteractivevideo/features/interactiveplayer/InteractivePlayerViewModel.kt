package ar.com.dmellano.exoplayerinteractivevideo.features.interactiveplayer

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ar.com.dmellano.exoplayerinteractivevideo.connection.NetworkHandler
import ar.com.dmellano.exoplayerinteractivevideo.connection.NetworkListener
import ar.com.dmellano.exoplayerinteractivevideo.domain.usecase.*
import ar.com.dmellano.exoplayerinteractivevideo.sensor.AccelerometerProvider
import ar.com.dmellano.exoplayerinteractivevideo.sensor.GyroscopeProvider
import ar.com.dmellano.exoplayerinteractivevideo.sensor.OrientationListener
import ar.com.dmellano.exoplayerinteractivevideo.sensor.ShakeListener
import com.google.android.exoplayer2.ExoPlayer
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class InteractivePlayerViewModel @Inject constructor(
    private val networkHandler: NetworkHandler,
    private val orientationProvider: GyroscopeProvider,
    private val accelerometerProvider: AccelerometerProvider,
    private val playVideoUseCase: PlayVideoUseCase,
    private val stopVideoUseCase: StopVideoUseCase,
    private val seekBackwardUseCase: SeekBackwardUseCase,
    private val seekForwardUseCase: SeekForwardUseCase,
    private val volumeUseCase: VolumeUseCase,
    private val initializePlayerUseCase: InitializePlayerUseCase,
    private val releasePlayerUseCase: ReleasePlayerUseCase
): ViewModel(), NetworkListener, OrientationListener, ShakeListener {

    data class MotionEvent(val xAxis: Int = 0, val yAxis: Int = 0, val zAxis: Int = 0, val isShaking: Boolean = false)

    private val _isConnected = MutableLiveData<Boolean>()
    val isConnected: LiveData<Boolean> get() = _isConnected

    private val _motionEvent = MutableLiveData<MotionEvent>()
    val motionEvent: LiveData<MotionEvent> get() = _motionEvent

    private val _player = MutableLiveData<ExoPlayer?>()
    val player: LiveData<ExoPlayer?> get() = _player

    init {
        orientationProvider.start()
        orientationProvider.orientationEventListener = this
        networkHandler.listener = this
        accelerometerProvider.start()
        accelerometerProvider.shakeListener = this
        onOrientationUpdate(0,0,0)
        _isConnected.value = networkHandler.isNetworkAvailable()
    }

    fun initializePlayer() {
        viewModelScope.launch {
            _player.postValue(initializePlayerUseCase.initialize())
        }
    }

    fun releasePlayer() {
        viewModelScope.launch {
            releasePlayerUseCase.release()
            _player.postValue(null)
        }
    }

    private fun updatePlayer() {
        networkHandler.isNetworkAvailable().let {
            _isConnected.postValue(it)
            if (it) {
                initializePlayer()
            } else {
                releasePlayer()
            }
        }
    }

    override fun onConnected() {
        updatePlayer()
    }

    override fun onDisconnected() {
        updatePlayer()
    }

    override fun onCleared() {
        orientationProvider.stop()
        accelerometerProvider.stop()
        super.onCleared()
    }

    override fun onOrientationUpdate(x: Int, y: Int, z: Int) {
        _motionEvent.postValue(
            MotionEvent(xAxis = x, yAxis = y, zAxis = z)
        )

        when {
            x > 0 -> {
                Log.d(TAG, "X Axis Positive Rotation: Volume Up")
                viewModelScope.launch {
                    volumeUseCase.volumeUp()
                }
            }
            x < 0 -> {
                Log.d(TAG, "X Axis Negative Rotation: Volume Down")
                viewModelScope.launch {
                    volumeUseCase.volumeDown()
                }
            }
        }
        when {
            z > 0 -> {
                Log.d(TAG, "Z Axis Positive Rotation: Seek Forward")
                viewModelScope.launch {
                    seekForwardUseCase.seekForward()
                }
            }
            z < 0 -> {
                Log.d(TAG, "Z Axis Negative Rotation: Seek Backward")
                viewModelScope.launch {
                    seekBackwardUseCase.seekBackward()
                }
            }
        }
    }

    override fun onShake() {
        Log.d(TAG, "onShake")
        _motionEvent.postValue(MotionEvent(isShaking = true))
        viewModelScope.launch {
            stopVideoUseCase.stopVideo()
        }
    }

    companion object {
        private const val TAG: String = "InteractivePlayerViewModel"
    }
}