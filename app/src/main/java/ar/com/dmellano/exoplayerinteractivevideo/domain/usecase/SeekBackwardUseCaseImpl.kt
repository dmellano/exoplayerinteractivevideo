package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager

class SeekBackwardUseCaseImpl constructor(private val playerManager: ExoplayerManager) : SeekBackwardUseCase {

    override suspend fun seekBackward() {
        playerManager.seekBackward()
    }
}