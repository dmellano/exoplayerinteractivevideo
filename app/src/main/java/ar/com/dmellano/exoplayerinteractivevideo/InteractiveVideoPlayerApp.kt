package ar.com.dmellano.exoplayerinteractivevideo

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class InteractiveVideoPlayerApp: Application() {
}