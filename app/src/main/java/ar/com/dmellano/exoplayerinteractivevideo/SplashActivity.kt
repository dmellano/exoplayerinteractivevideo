package ar.com.dmellano.exoplayerinteractivevideo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import ar.com.dmellano.exoplayerinteractivevideo.databinding.ActivitySplashBinding
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    private val activityScope = CoroutineScope(Dispatchers.Main)

    private val delay = 200

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val zoom: Animation = AnimationUtils.loadAnimation(this, R.anim.zoom)
        binding.imageLogo.startAnimation(zoom)

        zoom.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {
            }

            override fun onAnimationEnd(animation: Animation?) {
                activityScope.launch {
                    delay(800)
                    binding.imageLogo.visibility = View.GONE
                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
                    startActivity(intent)
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                    finish()
                }
            }

            override fun onAnimationStart(animation: Animation?) {
            }
        })
    }

    override fun onPause() {
        activityScope.cancel()
        super.onPause()
    }
}
