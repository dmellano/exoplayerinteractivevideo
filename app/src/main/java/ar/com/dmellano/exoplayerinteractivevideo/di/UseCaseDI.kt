package ar.com.dmellano.exoplayerinteractivevideo.di

import ar.com.dmellano.exoplayerinteractivevideo.domain.usecase.*
import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent


@InstallIn(SingletonComponent::class)
@Module
class MyModuleDI {

    @Provides
    fun providesPlayVideoUseCase(playerManager: ExoplayerManager): PlayVideoUseCase =
        PlayVideoUseCaseImpl(playerManager)

    @Provides
    fun providesStopVideoUseCase(playerManager: ExoplayerManager): StopVideoUseCase =
        StopVideoUseCaseImpl(playerManager)

    @Provides
    fun providesSeekForwardUseCase(playerManager: ExoplayerManager): SeekForwardUseCase =
        SeekForwardUseCaseImpl(playerManager)

    @Provides
    fun providesSeekBackwardUseCase(playerManager: ExoplayerManager): SeekBackwardUseCase =
        SeekBackwardUseCaseImpl(playerManager)

    @Provides
    fun providesVolumeUseCase(playerManager: ExoplayerManager): VolumeUseCase =
        VolumeUseCaseImpl(playerManager)

    @Provides
    fun providesInitializePlayerUseCase(playerManager: ExoplayerManager): InitializePlayerUseCase =
        InitializePlayerUseCaseImpl(playerManager)

    @Provides
    fun providesReleasePlayerUseCase(playerManager: ExoplayerManager): ReleasePlayerUseCase =
        ReleasePlayerUseCaseImpl(playerManager)
}