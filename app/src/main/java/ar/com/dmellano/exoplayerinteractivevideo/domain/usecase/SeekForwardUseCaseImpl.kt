package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager

class SeekForwardUseCaseImpl constructor(private val playerManager: ExoplayerManager) : SeekForwardUseCase {

    override suspend fun seekForward() {
        playerManager.seekForward()
    }
}