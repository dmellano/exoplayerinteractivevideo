package ar.com.dmellano.exoplayerinteractivevideo.player

import android.content.Context
import android.util.Log
import ar.com.dmellano.exoplayerinteractivevideo.R
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.audio.AudioAttributes
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import javax.inject.Inject


class ExoplayerManager @Inject constructor(val context: Context) {

    var player: ExoPlayer? = ExoPlayer.Builder(context).build()
    private var playWhenReady = true
    private var currentWindow = 0
    private var playbackPosition = 0L
    private var isPlayerPlaying = false

    private val playbackStateListener = object : Player.Listener {
        override fun onPlaybackStateChanged(playbackState: Int) {
            val stateString: String = when (playbackState) {
                ExoPlayer.STATE_IDLE -> "ExoPlayer.STATE_IDLE      -"
                ExoPlayer.STATE_BUFFERING -> "ExoPlayer.STATE_BUFFERING -"
                ExoPlayer.STATE_READY -> "ExoPlayer.STATE_READY     -"
                ExoPlayer.STATE_ENDED -> "ExoPlayer.STATE_ENDED     -"
                else -> "UNKNOWN_STATE             -"
            }
            isPlayerPlaying =
                playbackState == ExoPlayer.STATE_BUFFERING || playbackState == ExoPlayer.STATE_READY
            Log.d("InteractivePlayerFragment", "changed state to $stateString")
        }
    }

    fun initializePlayer(): ExoPlayer? {
        val trackSelector = DefaultTrackSelector(context).apply {
            setParameters(buildUponParameters().setMaxVideoSizeSd())
        }
        player = ExoPlayer.Builder(context)
            .setTrackSelector(trackSelector)
            .build()
            .also { exoPlayer ->
                val mediaItem = MediaItem.fromUri(context.getString(R.string.media_url_mp3))
                exoPlayer.setMediaItem(mediaItem)
                exoPlayer.playWhenReady = playWhenReady
                exoPlayer.seekTo(currentWindow, playbackPosition)
                exoPlayer.prepare()
            }
        player?.apply {
            setAudioAttributes(AudioAttributes.DEFAULT, true)
            addListener(playbackStateListener)
        }
        return player
    }

    fun releasePlayer() {
        player?.run {
            playbackPosition = this.currentPosition
            currentWindow = this.currentWindowIndex
            playWhenReady = this.playWhenReady
            release()
        }
        player = null
    }

    fun play() {
        player?.apply {
            Log.d(
                "ExoplayerManager",
                "play: isPlaying=$isPlayerPlaying / playbackState=$playbackState"
            )
            playWhenReady
        }
    }

    fun stopPlayer() {
        Log.d("ExoplayerManager", "stopPlayer: isPlaying=$isPlayerPlaying")
        if (isPlayerPlaying)
            player?.pause()
    }

    fun seekForward() {
        if (isPlayerPlaying) {
            player?.apply {
                seekTo(
                    currentMediaItemIndex,
                    currentPosition.plus(duration.seekIncrement())
                ) // +3%
            }
        }
    }

    fun seekBackward() {
        if (isPlayerPlaying) {
            player?.apply {
                seekTo(
                    currentMediaItemIndex,
                    currentPosition.minus(duration.seekIncrement())
                ) // -3%
            }
        }
    }

    fun volumeUp() {
        if (isPlayerPlaying)
            player?.decreaseDeviceVolume()
    }

    fun volumeDown() {
        if (isPlayerPlaying)
            player?.increaseDeviceVolume()
    }

    private fun Long.seekIncrement(): Long {
        return (this * SEEK_INCREMENT_DECREMENT_PERCENTAGE).toLong()
    }

    companion object {
        private const val SEEK_INCREMENT_DECREMENT_PERCENTAGE = 0.03f
    }
}

