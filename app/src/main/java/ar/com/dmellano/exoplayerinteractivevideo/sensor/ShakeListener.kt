package ar.com.dmellano.exoplayerinteractivevideo.sensor

interface ShakeListener {
    fun onShake()
}