package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

interface SeekForwardUseCase {
    suspend fun seekForward()
}