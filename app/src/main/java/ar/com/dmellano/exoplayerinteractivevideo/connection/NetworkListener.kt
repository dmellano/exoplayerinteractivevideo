package ar.com.dmellano.exoplayerinteractivevideo.connection

interface NetworkListener {
    fun onConnected()
    fun onDisconnected()
}