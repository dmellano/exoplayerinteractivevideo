package ar.com.dmellano.exoplayerinteractivevideo.di

import android.content.Context
import ar.com.dmellano.exoplayerinteractivevideo.connection.NetworkHandler
import ar.com.dmellano.exoplayerinteractivevideo.sensor.GyroscopeProvider
import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager
import ar.com.dmellano.exoplayerinteractivevideo.sensor.AccelerometerProvider

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(ViewModelComponent::class)
@Module
class ServicesDI {

    @Provides
    @ViewModelScoped
    fun provideGyroscopeProvider(@ApplicationContext appContext: Context) = GyroscopeProvider(context = appContext)

    @Provides
    @ViewModelScoped
    fun provideAccelerometerProvider(@ApplicationContext appContext: Context) = AccelerometerProvider(context = appContext)
}

@InstallIn(SingletonComponent::class)
@Module
class AppServicesDI {

    @Provides
    @Singleton
    fun provideNetworkHandler(@ApplicationContext appContext: Context) = NetworkHandler(appContext)


    @Provides
    @Singleton
    fun provideExoPlayerManager(@ApplicationContext appContext: Context) = ExoplayerManager(context = appContext)

}
