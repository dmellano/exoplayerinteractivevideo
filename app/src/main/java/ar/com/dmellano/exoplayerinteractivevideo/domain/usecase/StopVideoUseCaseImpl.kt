package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import ar.com.dmellano.exoplayerinteractivevideo.player.ExoplayerManager

class StopVideoUseCaseImpl constructor(private val playerManager: ExoplayerManager) :
    StopVideoUseCase {

    override suspend fun stopVideo() {
        playerManager.stopPlayer()
    }
}