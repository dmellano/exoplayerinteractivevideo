package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

interface ReleasePlayerUseCase {
    suspend fun release()
}