package ar.com.dmellano.exoplayerinteractivevideo.domain.usecase

import com.google.android.exoplayer2.ExoPlayer

interface PlayVideoUseCase {
    suspend fun playVideo()
}
