package ar.com.dmellano.exoplayerinteractivevideo.features.interactiveplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import ar.com.dmellano.exoplayerinteractivevideo.R
import ar.com.dmellano.exoplayerinteractivevideo.databinding.InteractivePlayerFragmentBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class InteractivePlayerFragment : Fragment(R.layout.interactive_player_fragment) {
    private val addLoadViewModel: InteractivePlayerViewModel by viewModels()
    private var binding: InteractivePlayerFragmentBinding? = null

    override fun onResume() {
        super.onResume()
        addLoadViewModel.initializePlayer()
    }

    override fun onStop() {
        super.onStop()
        addLoadViewModel.releasePlayer()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return super.onCreateView(inflater, container, savedInstanceState).apply {
            this?.apply {
                binding = InteractivePlayerFragmentBinding.bind(this)
            }

            lifecycleScope.launchWhenStarted {
                addLoadViewModel.player.observe(
                    this@InteractivePlayerFragment.viewLifecycleOwner,
                    Observer {
                        binding?.videoView?.player = it
                    }
                )
                addLoadViewModel.isConnected.observe(
                this@InteractivePlayerFragment.viewLifecycleOwner,
                Observer {
                    binding?.apply {
                        this.noConnectionContainer.visibility =
                            if (it) View.GONE else View.VISIBLE
                    }
                })
                addLoadViewModel.motionEvent.observe(
                    this@InteractivePlayerFragment.viewLifecycleOwner,
                    Observer { motionEvent ->
                        binding?.apply {
                            this.orientationXAxisTxtVw.text =
                                motionEvent.xAxis.toAxisLegend("x")
                            this.orientationYAxisTxtVw.text =
                                motionEvent.yAxis.toAxisLegend("y")
                            this.orientationZAxisTxtVw.text =
                                motionEvent.zAxis.toAxisLegend("z")
                        }
                        if (motionEvent.isShaking)
                            Toast.makeText(requireContext(), getText(R.string.is_shaking), Toast.LENGTH_LONG).show()
                    })
            }
        }
    }
}


fun Int.toAxisLegend(axis: String): String {
    return "${axis.uppercase()} Axis: ${
        when {
            this > 0 -> "+${axis.uppercase()}"
            this < 0 -> "-${axis.uppercase()}"
            else -> "Static"
        }
    }"
}