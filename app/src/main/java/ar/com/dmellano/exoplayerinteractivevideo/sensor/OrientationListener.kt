package ar.com.dmellano.exoplayerinteractivevideo.sensor

interface OrientationListener {
    fun onOrientationUpdate(x: Int, y: Int, z: Int)
}